package com.sf.myapplication;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button getButton = findViewById(R.id.login_button);
        getButton.setEnabled(true);
        getButton.setOnClickListener(playWelcomeFileListener);
    }

    private View.OnClickListener playWelcomeFileListener = new View.OnClickListener() {
        public void onClick(View view) {
            System.out.println("--------- playWelcomeFileListener is Called ------------");
            MediaPlayer mp = MediaPlayer.create(MainActivity.this , R.raw.song1);
            mp.start();
            recordUserSpeechActivity(view);
        }
    };

    public void recordUserSpeechActivity(View view) {
        System.out.println("--------- Hey Speech Listener is called ----------");
        Intent intent = new Intent(this, SpeechListenerActivity.class);
        startActivity(intent);
    }
}
